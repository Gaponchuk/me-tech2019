function initMap() {
    var uluru = {lat: 26.228391, lng: 50.585975};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12.5,
      center: uluru,
      styles: [
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.business",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.school",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
]
    });

    var infowindow = new google.maps.InfoWindow({
      content: 'Euro Petroleum Consultants'
    });

    var marker = new google.maps.Marker({
      position: uluru,
      map: map
    });

    marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
  }