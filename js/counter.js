 // start counter
var counter = document.querySelector('#counter');
var counterTop = counter.offsetTop;


function counterRun() {
  if(window.scrollY >= counterTop/2) {
    
    $('.indicators__value').each(function() {
      var $this = $(this),
      countTo = $this.attr('data-count');
  
      $({ countNum: $this.text()}).animate({
        countNum: countTo
      },

      {
        duration: 2000,
        easing:'linear',
        step: function() {
          $this.text(Math.floor(this.countNum));
        },
        complete: function() {
          $this.text(this.countNum);
        }
      });  
    });

  }
}

window.addEventListener('scroll', counterRun);



